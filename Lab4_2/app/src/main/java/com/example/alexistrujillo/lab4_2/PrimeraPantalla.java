package com.example.alexistrujillo.lab4_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class PrimeraPantalla extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primera_pantalla);
    }

    public void salta (View view) {
        Log.d("App", "Se ha hecho click");
        Intent intent= new Intent(this, Segunda.class);
        intent.putExtra("valorTest","false");
        startActivity(intent);
    }
    
}
