package com.example.alexistrujillo.lab4_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Segunda extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);


        String varString = getIntent().getStringExtra("valorTest");
        Log.d("HelloWorld-SecActivity",varString);
    }

    public void volver(View view) {
        Log.d("App","Adios con Dios");
        Intent intent = new Intent(this, PrimeraPantalla.class);
        startActivity(intent);
        finish();

    }
}
